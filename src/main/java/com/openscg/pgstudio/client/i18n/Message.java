package com.openscg.pgstudio.client.i18n;

import com.google.gwt.core.client.GWT;
import com.google.gwt.i18n.client.Messages;

public interface Message extends Messages {

	public class Util {
		public static Message MESSAGE = GWT.create(Message.class);
	}

	// ColumnPanel
	public String Table_comment();

	public String View_comment();

	public String Comment();

	public String Drop_Column();

	public String Create_Column();

	public String Rename_Column();

	public String Column_Name();

	public String Data_Type();

	public String Nullable();

	public String Default();

	public String Column_comment();

	public String Refresh();

	// ConstraintPanel
	public String Drop_Constraint();

	public String Create_Constraint();

	public String Rename_Constraint();

	public String Constraint_Name();

	public String Type();

	public String Deferrable();

	public String Deferred();

	public String Update_Action();

	public String Delete_Action();

	public String Constraint_Definition();

	// DetailsTabPanel
	public String Columns();

	public String Indexes();

	public String Constraints();

	public String Triggers();

	public String Rules();

	public String Data();

	public String Stats();

	public String Script();

	public String Security();

	public String Policies();

	// IndexPanel
	public String Drop_Index();

	public String Create_Index();

	public String Rename_Index();

	public String Index_Name();

	public String Access_Method();

	public String Primary_Key();

	public String Unique();

	public String Partial();

	public String Index_Statistics();

	public String Index_Definition();

	// ItemDataPanel
	public String Loading_Data();

	// MonitorPanel
	public String Refresh_Rate();

	public String Database_Name();

	public String User_Name();

	public String State();

	public String Client_Address();

	public String Backend_Start();

	public String Transaction_Start();

	public String Query_Time();

	public String Query();

	// PolicyPanel
	public String Drop_Policy();

	public String Rename_Policy();

	public String Create_Policy();

	public String Name();

	public String Command();

	public String Roles();

	public String Read_Expression_Using(); // "Read Expression (Using)"

	public String Write_Expression_With_Check(); // "Write Expression (With Check)"

	// RulePanel
	public String Drop_Rule();

	public String Create_Rule();

	public String Rule_Name();

	public String Enabled();

	public String Instead();

	public String Rule_Definition();

	// ScriptPanel

	// SecurityPanel
	public String Revoke_Privilege();

	public String Add_Privileges();

	public String Grantee();

	public String Privilege();

	public String Grantable();

	public String Grantor();

	// SQLWorksheet
	public String Results();

	public String Messages();

	public String Explain();

	public String Limit();

	public String No_Limit();

	public String Invalid_file_type();

	public String Problem_reading_file();

	public String Save();

	public String Open();

	public String Stop();

	// StatsPanel
	
	// TriggerPanel
	public String Drop_Trigger();
	
	public String Create_Trigger();
	
	public String Rename_Trigger();
	
	public String Trigger_Name();
	
	public String Initially_Deferred();
	
	public String Trigger_Definition();
}
