/*
 * PostgreSQL Studio
 */
package com.openscg.pgstudio.server;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.openscg.pgstudio.server.login.ApplicationUser;
import com.openscg.pgstudio.server.login.JsonRpcAccountAuthenticator;
import com.openscg.pgstudio.server.login.LoginData;
import com.openscg.pgstudio.server.login.LoginException;
import com.openscg.pgstudio.server.util.ConnectionInfo;
import com.openscg.pgstudio.server.util.ConnectionManager;
import com.openscg.pgstudio.server.util.ContextUtil;
import com.openscg.pgstudio.shared.DatabaseConnectionException;

public class LoginHandler extends HttpServlet {

	private static final long serialVersionUID = -3598253864439571563L;

	public void doPost(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException {
		res.setContentType("text/html");

		String clientIP = ConnectionInfo.remoteAddr(req);
		String userAgent = req.getHeader("User-Agent");
		String queryString = req.getQueryString();

		String username = req.getParameter("username");
		String passwd = req.getParameter("password");

		HttpSession session = req.getSession(true);

		LoginData login = new LoginData(username, passwd);

		JsonRpcAccountAuthenticator authenticator = new JsonRpcAccountAuthenticator();
		ApplicationUser user;
		try {
			user = authenticator.login(login);

			if (user.hasGroup("whirl-application-editor")) {
				String databaseUrl = ContextUtil
						.lookup("Database/connection/url");
				URI uri = new URI(databaseUrl.substring(5));
				String dbURL = uri.getHost();
				int dbPort = uri.getPort() == -1 ? 5432 : uri.getPort();
				String dbName = uri.getPath().substring(1);
				String dbUser = ContextUtil
						.lookup("Database/connection/username");
				String dbPassword = ContextUtil
						.lookup("Database/connection/password");

				String token = "";

				ConnectionManager connMgr = new ConnectionManager();

				try {
					token = connMgr.addConnection(dbURL, dbPort, dbName,
							dbUser, dbPassword, clientIP, userAgent);
				} catch (DatabaseConnectionException e) {
					session.setAttribute("err_msg", e.getMessage());
					// TODO remove
					session.setAttribute("dbName", dbName);
					session.setAttribute("dbURL", dbURL);
					session.setAttribute("username", username);
				}

				if (!token.equals("")) {
					session.setAttribute("dbToken", token);
					session.setAttribute("dbVersion",
							Integer.toString(connMgr.getDatabaseVersion()));
				}
			} else {
				session.setAttribute("err_msg", "User has no access");
				session.setAttribute("username", username);
			}
		} catch (LoginException | URISyntaxException e) {
			session.setAttribute("err_msg", e.getMessage());
			session.setAttribute("username", username);
		}

		// Try redirecting the client to the page he first tried to access
		try {
			String target = this.getServletContext().getInitParameter(
					"home_url");

			if (queryString != null)
				target = target + "?" + queryString;

			res.sendRedirect(target);
			return;
		} catch (Exception ignored) {
		}

	}
}