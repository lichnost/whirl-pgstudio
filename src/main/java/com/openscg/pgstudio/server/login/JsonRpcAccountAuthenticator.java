package com.openscg.pgstudio.server.login;

import java.net.URL;
import java.util.Set;

import com.googlecode.jsonrpc4j.JsonRpcHttpClient;
import com.openscg.pgstudio.server.util.ContextUtil;

public class JsonRpcAccountAuthenticator implements AccountAuthenticator {

	private static String getServiceURL() {
		return ContextUtil.lookup("Sprint/portal/service/url");
	}

	private static String getServiceLogin() {
		return ContextUtil.lookup("Sprint/portal/service/login");
	}

	private static String getServicePassword() {
		return ContextUtil.lookup("Sprint/portal/service/password");
	}

	private static String getPlatformId() {
		return ContextUtil.lookup("Sprint/portal/platform-id");
	}

	public static class JsonRpcLoginRequest {
		private String platformId;
		private String portalLogin;
		private String portalPassword;
		private LoginData data;

		public JsonRpcLoginRequest() {
		}

		public String getPlatformId() {
			return platformId;
		}

		public void setPlatformId(String platformId) {
			this.platformId = platformId;
		}

		public String getPortalLogin() {
			return portalLogin;
		}

		public void setPortalLogin(String portalLogin) {
			this.portalLogin = portalLogin;
		}

		public String getPortalPassword() {
			return portalPassword;
		}

		public void setPortalPassword(String portalPassword) {
			this.portalPassword = portalPassword;
		}

		public LoginData getData() {
			return data;
		}

		public void setData(LoginData data) {
			this.data = data;
		}

	}

	public static class JsonRpcLoginResponse {
		private String id;
		private String name;
		private String comment;
		private boolean authorized = false;
		private String login;
		private String email;
		private Set<String> groups;

		public JsonRpcLoginResponse() {
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getComment() {
			return comment;
		}

		public void setComment(String comment) {
			this.comment = comment;
		}

		public boolean isAuthorized() {
			return authorized;
		}

		public void setAuthorized(boolean authorized) {
			this.authorized = authorized;
		}

		public String getLogin() {
			return login;
		}

		public void setLogin(String login) {
			this.login = login;
		}

		public String getEmail() {
			return email;
		}

		public void setEmail(String email) {
			this.email = email;
		}

		public Set<String> getGroups() {
			return groups;
		}

		public void setGroups(Set<String> groups) {
			this.groups = groups;
		}

	}

	@Override
	public ApplicationUser login(LoginData login) throws LoginException {
		try {
			JsonRpcHttpClient client = new JsonRpcHttpClient(new URL(
					getServiceURL()));

			JsonRpcLoginRequest request = new JsonRpcLoginRequest();
			request.setPortalLogin(getServiceLogin());
			request.setPortalPassword(getServicePassword());
			request.setPlatformId(getPlatformId());
			request.setData(login);

			JsonRpcLoginResponse response = client.invoke("login", request,
					JsonRpcLoginResponse.class);

			ApplicationUser user = new ApplicationUser();
			if (response.isAuthorized()) {
				user.setId(response.getId());
				user.setLogin(response.getLogin());
				user.setName(response.getName());
				user.addGroups(response.getGroups());
			} else {
				throw new LoginException("Not authorized");
			}
			return user;
		} catch (Throwable e) {
			throw new LoginException(e.getMessage(), e);
		}
	}

	@Override
	public void logout(ApplicationUser user) {
	}

}
