package com.openscg.pgstudio.server.login;

public interface AccountAuthenticator {

	ApplicationUser login(LoginData login) throws LoginException;

	void logout(ApplicationUser user) throws LoginException;

}