package com.openscg.pgstudio.server.login;

import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

/** Пользователь */
public class ApplicationUser implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1808419282462455596L;

	private transient Map<String, String> contextMap = new HashMap<String, String>();

	/**
	 * Идентификатор пользователя
	 */
	private String id;

	/**
	 * ФИО
	 * 
	 */
	private String name;

	/** Запрашиваемый URL */
	private String requesUrl;

	/** Гостевой URL */
	private String guestURL;

	private boolean guest;

	/** Логин */
	private String login;

	private String ip;
	private String hostname;

	private Locale locale;
	private TimeZone timeZone;

	private Set<String> groups = new HashSet<String>();
	private Map<String, Object> javaObjects = new HashMap<String, Object>();

	public ApplicationUser() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setLocale(Locale locale) {
		this.locale = locale;
	}

	public Locale getLocale() {
		return locale;
	}

	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

	public TimeZone getTimeZone() {
		return timeZone;
	}

	public String getRequestUrl() {
		return requesUrl;
	}

	public void setRequestUrl(String requestUrl) {
		requesUrl = requestUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setGuest(boolean guest) {
		this.guest = guest;
	}

	public boolean isGuest() {
		return guest;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public void setContextParameter(String key, String value) {
		contextMap.put(key, value);
	}

	public void removeContextParameter(String key) {
		contextMap.remove(key);
	}

	public void clearContextParameters() {
		contextMap.clear();
	}

	public Map<String, String> getContextParameters() {
		return Collections.unmodifiableMap(contextMap);
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public String getIp() {
		return ip;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public String getHostname() {
		return hostname;
	}

	public void addGroup(String group) {
		if (group == null || group.trim().isEmpty()) {
			return;
		}
		groups.add(group.trim());
	}

	public void addGroups(Collection<String> groups) {
		for (String g : groups) {
			addGroup(g);
		}
	}

	public void removeGroup(String group) {
		if (group == null || group.trim().isEmpty()) {
			return;
		}
		groups.remove(group.trim());
	}

	public void clearGroups() {
		groups.clear();
	}

	public boolean hasGroups() {
		return !groups.isEmpty();
	}

	public boolean hasGroup(String group) {
		if (group == null || group.trim().isEmpty()) {
			return false;
		}
		return groups.contains(group.trim());
	}

	public Collection<String> getGroups() {
		return Collections.unmodifiableSet(groups);
	}

	public boolean hasJavaObject(String name) {
		return javaObjects.containsKey(name);
	}

	public void saveJavaObject(String name, Object object) {
		javaObjects.put(name, object);
	}

	public Object loadJavaObject(String name) {
		return javaObjects.get(name);
	}

	public Object removeJavaObject(String name) {
		return javaObjects.remove(name);
	}

}
